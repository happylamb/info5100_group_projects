/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface;

import business.AirDoctorVolunteerPlatform;
import business.db4oUtil.DB4OUtil;
import business.useraccount.UserAccount;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import javax.swing.JPanel;
import userInterface.PharmaceuticalManufacturer.PharmManfAdminButtonJPanel;
import userInterface.PharmaceuticalManufacturer.charity.CharityButtonJPanel;
import userInterface.government.VolunteerFinance.VolunteerFinanceButtonJPanel;
import userInterface.government.VolunteerFinance.VolunteerFinanceWorkAreaJPanel;
import userInterface.patients.PatientsButtonJPanel;
import userInterface.patients.PatientsWorkAreaJPanel;
import userInterface.signup.SignUpBtnJPanel;
import userInterface.signup.SignUpTypeJPanel;
import userInterface.systemAdmin.SystemAdminButtonJPanel;
import userInterface.systemAdmin.SystemAdminWorkAreaJPanel;
import userInterface.volunteer.VolunteerAdminButtonJPanel;
import userInterface.volunteer.VolunteerAdminWorkAreaJPanel;
import userInterface.volunteer.doctor.DoctorButtonJPanel;
import userInterface.volunteer.doctor.DoctorQuestionsJPanel;
import userInterface.volunteer.doctor.DoctorWorkAreaJPanel;
import userInterface.volunteer.finance.FinanceButtonJPanel;
import userInterface.volunteer.finance.FinanceWorkAreaJPanel;
import userInterface.volunteer.medicineManage.MedicineManageButtonJPanel;


/**
 *
 * @author gaomc
 */
public class MainJFrame extends javax.swing.JFrame {

    private DB4OUtil dB4oUtil ;
    private AirDoctorVolunteerPlatform system;
    private UserAccount userAccount;
    /**
     * Creates new form MainJFrame
     * @param userAccount
     */
    
    // to implement the draging fuction
    // add upJPanelMousePressed / upJPanelMouseDragged
    int xx = 0;
    int xy = 0;

    public MainJFrame(UserAccount userAccount) {
        initComponents();
        this.dB4oUtil = DB4OUtil.getInstance();
        this.userAccount = userAccount;
        this.system = AirDoctorVolunteerPlatform.getInstance();
        setButton();
    }
 
    private void setButton(){
        
        if(this.userAccount.getUsername() == null){
            SignUpBtnJPanel signUpBtnJPanel = new SignUpBtnJPanel(mainJSplitPane);
            SignUpTypeJPanel signUpTypeJPanel = new SignUpTypeJPanel(mainJSplitPane);
            setBothSideJPanel(signUpBtnJPanel,signUpTypeJPanel);
            
        }else{
            String role = this.userAccount.getRole().toString();
            System.out.println(role);
            switch (role) {
                case "SystemAdministration":
                    SystemAdminButtonJPanel sysAdminJPanel = new SystemAdminButtonJPanel(this.userAccount,mainJSplitPane);
                    SystemAdminWorkAreaJPanel sysAdminWorkAreaJPanel = new SystemAdminWorkAreaJPanel(this.userAccount,mainJSplitPane);
                    setBothSideJPanel(sysAdminJPanel,sysAdminWorkAreaJPanel);
                    break;
                case "Patient":
                    PatientsButtonJPanel patientBtnJPanel = new PatientsButtonJPanel(this.userAccount,mainJSplitPane);
                    PatientsWorkAreaJPanel patientWorkAreaJPanel = new PatientsWorkAreaJPanel(this.userAccount, mainJSplitPane);
                    setBothSideJPanel(patientBtnJPanel,patientWorkAreaJPanel);
                    break;
                case "VolunteerDoctor":
                    DoctorButtonJPanel doctorBtnJPanel = new DoctorButtonJPanel(this.userAccount,mainJSplitPane);
                    
//                    DoctorWorkAreaJPanel doctorWork = new DoctorWorkAreaJPanel(this.userAccount,mainJSplitPane);
                    DoctorQuestionsJPanel doctorQuestionsJPanel = new DoctorQuestionsJPanel(userAccount);

                    setBothSideJPanel(doctorBtnJPanel, doctorQuestionsJPanel);
                    mainJSplitPane.setLeftComponent(doctorBtnJPanel);
                    break;
                case "Finance":
                    FinanceButtonJPanel financeBtnJPanel = new FinanceButtonJPanel(this.userAccount,mainJSplitPane);
                    mainJSplitPane.setLeftComponent(financeBtnJPanel);
                    FinanceWorkAreaJPanel checkDonation = new FinanceWorkAreaJPanel(userAccount,mainJSplitPane);
                    mainJSplitPane.setRightComponent(checkDonation);
                    break;
                case "MedicineManage":
                    MedicineManageButtonJPanel medicineManageButtonJPanel = new MedicineManageButtonJPanel(this.userAccount,mainJSplitPane);
                    mainJSplitPane.setLeftComponent(medicineManageButtonJPanel);
                    break;
                case "PharmacyAdmin":
                    PharmManfAdminButtonJPanel pharmacyAdminJPanel = new PharmManfAdminButtonJPanel(this.mainJSplitPane, this.userAccount);
                    mainJSplitPane.setLeftComponent(pharmacyAdminJPanel);
                    break;
                case "Charity":
                    CharityButtonJPanel charityButtonPanel = new CharityButtonJPanel(this.mainJSplitPane, this.userAccount);
                    mainJSplitPane.setLeftComponent(charityButtonPanel);
                    break;
                case "VolunteerAdmin":
                    VolunteerAdminButtonJPanel volunteerAdminButtonJPanel = new VolunteerAdminButtonJPanel(this.mainJSplitPane, this.userAccount);
                    VolunteerAdminWorkAreaJPanel volunteerAdminWorkAreaJPanel = new VolunteerAdminWorkAreaJPanel(this.mainJSplitPane, this.userAccount);
                    mainJSplitPane.setLeftComponent(volunteerAdminButtonJPanel);
                    mainJSplitPane.setRightComponent(volunteerAdminWorkAreaJPanel);
                    break;
                case "VolunteerFinance": 
                    VolunteerFinanceButtonJPanel volunteerFinanceButtonJPanel = new VolunteerFinanceButtonJPanel(this.mainJSplitPane, this.userAccount);
                    VolunteerFinanceWorkAreaJPanel volunteerFinanceWorkAreaJPanel = new VolunteerFinanceWorkAreaJPanel(this.userAccount, this.mainJSplitPane);
                    setBothSideJPanel(volunteerFinanceButtonJPanel,volunteerFinanceWorkAreaJPanel);
                    break;
                default:
                    break;
            }
        }

    }
    
    private void setBothSideJPanel(JPanel btnSideJPanel, JPanel workAreaSideJPanel){
         mainJSplitPane.setLeftComponent(btnSideJPanel);
         mainJSplitPane.setRightComponent(workAreaSideJPanel);
    }
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        upJSplitPane = new javax.swing.JSplitPane();
        upJPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        btn_exit = new javax.swing.JLabel();
        mainJPanel = new javax.swing.JPanel();
        mainJSplitPane = new javax.swing.JSplitPane();
        btnJPanel = new javax.swing.JPanel();
        cardSequenceJPanel = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setBounds(new java.awt.Rectangle(80, 100, 1112, 1000));
        setMinimumSize(new java.awt.Dimension(800, 600));
        setUndecorated(true);
        setSize(new java.awt.Dimension(1112, 1000));
        setType(java.awt.Window.Type.POPUP);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        upJSplitPane.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        upJSplitPane.setDividerLocation(80);
        upJSplitPane.setDividerSize(1);
        upJSplitPane.setForeground(new java.awt.Color(255, 255, 255));
        upJSplitPane.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        upJSplitPane.setMinimumSize(new java.awt.Dimension(0, 0));
        upJSplitPane.setPreferredSize(new java.awt.Dimension(1300, 900));

        upJPanel.setBackground(new java.awt.Color(84, 107, 130));
        upJPanel.setMinimumSize(new java.awt.Dimension(0, 0));
        upJPanel.setPreferredSize(new java.awt.Dimension(1200, 80));
        upJPanel.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                upJPanelMouseDragged(evt);
            }
        });
        upJPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                upJPanelMousePressed(evt);
            }
        });
        upJPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Air Health Platform");
        upJPanel.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 30, -1, -1));

        btn_exit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8_Exit_25px.png")));
        btn_exit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btn_exitMousePressed(evt);
            }
        });
        upJPanel.add(btn_exit, new org.netbeans.lib.awtextra.AbsoluteConstraints(1020, 30, 30, 30));

        upJSplitPane.setLeftComponent(upJPanel);

        mainJPanel.setBackground(new java.awt.Color(255, 255, 255));
        mainJPanel.setLayout(new java.awt.CardLayout());

        mainJSplitPane.setDividerLocation(150);
        mainJSplitPane.setDividerSize(1);

        btnJPanel.setBackground(new java.awt.Color(255, 255, 255));
        btnJPanel.setPreferredSize(new java.awt.Dimension(120, 263));
        btnJPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        mainJSplitPane.setLeftComponent(btnJPanel);

        cardSequenceJPanel.setBackground(new java.awt.Color(255, 255, 255));
        cardSequenceJPanel.setPreferredSize(new java.awt.Dimension(1000, 200));
        cardSequenceJPanel.setLayout(new javax.swing.OverlayLayout(cardSequenceJPanel));
        mainJSplitPane.setRightComponent(cardSequenceJPanel);

        mainJPanel.add(mainJSplitPane, "card2");

        upJSplitPane.setRightComponent(mainJPanel);

        getContentPane().add(upJSplitPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(-4, -4, 1200, 590));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_exitMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_exitMousePressed
        // TODO add your handling code here:
        LoginJFrame login = new LoginJFrame();
        login.setVisible(true);
        dB4oUtil.storeSystem(system);

        this.dispose();
        
    }//GEN-LAST:event_btn_exitMousePressed

    private void upJPanelMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_upJPanelMousePressed
        // TODO add your handling code here:
        xx = evt.getX();
        xy = evt.getY();
    }//GEN-LAST:event_upJPanelMousePressed

    private void upJPanelMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_upJPanelMouseDragged
        // TODO add your handling code here:
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();
        this.setLocation(x - xx, y - xy);
    }//GEN-LAST:event_upJPanelMouseDragged

   
//    /**
//     * @param args the command line arguments
//     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new MainJFrame().setVisible(true);
//            }
//        });
//    }
    
    private void setColor(JPanel pane)
    {
        pane.setBackground(new Color(41,57,80));
    }
    
    private void resetColor(JPanel [] pane, JPanel [] indicators)
    {
        for(int i=0;i<pane.length;i++){
           pane[i].setBackground(new Color(23,35,51));
           
        } for(int i=0;i<indicators.length;i++){
           indicators[i].setOpaque(false);           
        }
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel btnJPanel;
    private javax.swing.JLabel btn_exit;
    private javax.swing.JPanel cardSequenceJPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel mainJPanel;
    private javax.swing.JSplitPane mainJSplitPane;
    private javax.swing.JPanel upJPanel;
    private javax.swing.JSplitPane upJSplitPane;
    // End of variables declaration//GEN-END:variables
}

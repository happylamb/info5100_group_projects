/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.PharmaceuticalManufacturer.charity;

import business.AirDoctorVolunteerPlatform;
import business.VolunteerMedicineInventory;
import business.enterprise.Enterprise;
import business.network.Network;
import business.organization.pharmacy.CharityOrganization;
import business.user.charity.MedicineList;
import business.useraccount.UserAccount;
import business.workqueue.WorkRequest;
import business.workqueue.MedicineRequest;
import business.workqueue.WorkQueue;
import java.awt.CardLayout;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import javax.mail.AuthenticationFailedException;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSplitPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author gaomc
 */
public class CharityWorkAreaJPanel extends javax.swing.JPanel {

    /**
     * Creates new form charityWorkAreaJPanel
     */
    private JPanel userProcessJPanel;
    private AirDoctorVolunteerPlatform system;
    private Enterprise enterprise;
    private UserAccount userAccount;
    private MedicineList list;
    private JSplitPane mainJSplitPane;
    private Network network;
    private WorkQueue workQueue;
    private VolunteerMedicineInventory inventory;
    private String myAddress;
    private String pharmacyName;


    public CharityWorkAreaJPanel(JPanel container,UserAccount account, CharityOrganization organization, Enterprise enterprise) {
        initComponents();
        

    }
    public CharityWorkAreaJPanel(UserAccount account,JSplitPane mainJSplitPane){
        initComponents();
        this.userAccount = account;
        this.mainJSplitPane = mainJSplitPane;
        this.network = account.getNetwork();
        System.out.println(network.getName());
        System.out.println("getWQ: " + account.getNetwork().getSepcificWorkQueue("notification_Pharmacy"));
        this.workQueue = account.getNetwork().getSepcificWorkQueue("notification_Pharmacy");
        this.inventory = account.getNetwork().getVolunteerMedicineInventory();
        this.myAddress = userAccount.getUser().getEmail();
        this.pharmacyName = userAccount.getUser().getName();
        populateRequestTable();
    }


    public void populateRequestTable(){
        DefaultTableModel model = (DefaultTableModel) listTable.getModel();

        model.setRowCount(0);
        for (WorkRequest request : workQueue.getWorkRequestList()){
            MedicineRequest medicineRequest = (MedicineRequest)request;
            if(request.getStatus().equals("unhandled")){
            Object[] row = new Object[4];
            row[0] = medicineRequest;
            row[1] = medicineRequest.getQuantity();
            row[2] = medicineRequest.getRequestDate();
            row[3] = false;


            model.addRow(row);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        listTable = new javax.swing.JTable();
        freshBtn = new javax.swing.JButton();
        processBtn = new javax.swing.JButton();
        jPanel14 = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        listTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "medicine name", "quantity", "date", "select"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(listTable);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 60, 370, 240));

        freshBtn.setBackground(new java.awt.Color(255, 255, 255));
        freshBtn.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        freshBtn.setText("Refresh");
        freshBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                freshBtnActionPerformed(evt);
            }
        });
        add(freshBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 190, -1, 50));

        processBtn.setBackground(new java.awt.Color(255, 255, 255));
        processBtn.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        processBtn.setText("Process");
        processBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                processBtnActionPerformed(evt);
            }
        });
        add(processBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 250, -1, 50));

        jPanel14.setBackground(new java.awt.Color(120, 168, 251));

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 91, Short.MAX_VALUE)
        );

        add(jPanel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        jPanel13.setBackground(new java.awt.Color(84, 127, 206));

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );

        add(jPanel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void processBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_processBtnActionPerformed
        // TODO add your handling code here:
        DefaultTableModel model = (DefaultTableModel)listTable.getModel();
        int j = 3;
        Date date = new Date();

        ArrayList<MedicineRequest> requestList = new ArrayList<>();
        
        for(int i = 0;i<model.getRowCount();i++){
            if((Boolean)model.getValueAt(i, j).equals(true)){
                MedicineRequest request = (MedicineRequest) listTable.getValueAt(i, 0);
                request.setStatus("completed");
                request.setResolveDate(date);
                request.setReceiver(userAccount);
                String name = request.getMedicineName();
                int number = request.getQuantity();

                
                requestList.add(request);
                System.out.println(request.getMedicineName()+"bianli test");
                //change the related medicine stock in inventory
                
                if(inventory.getMedicineInventory().containsKey(name)){
                    number += inventory.getMedicineInventory().get(name);
                    inventory.getMedicineInventory().put(name, number);
                    
                    System.out.println("Inventory UPdate: " + inventory.getMedicineInventory());
               }else{
                   inventory.getMedicineInventory().put(name, number);
                   System.out.println("Inventory UPdate2: " + inventory.getMedicineInventory());

                }
            }
        }
        
        //send email
        String[] temp = myAddress.split("@");
        String username = temp[0];
        int result = JOptionPane.showConfirmDialog(null, "Processed! Do you want to sent a confirmation email?");
        if(result == 0){
            JPasswordField pw = new JPasswordField();
            JOptionPane.showMessageDialog(null, pw, "Please Input your Password to Verify!", JOptionPane.PLAIN_MESSAGE);
            String inputValue = String.valueOf(pw.getPassword());
        for(MedicineRequest eRequest:requestList){
           String receiveAddress = eRequest.getSender().getUser().getEmail();
           String medicineName = eRequest.getMedicineName();
           int eNumeber = eRequest.getQuantity();
            System.out.println(medicineName+" "+eNumeber+" "+receiveAddress+"email test");
           sendMail(myAddress,receiveAddress,username,inputValue,medicineName,eNumeber,pharmacyName);
           }
        }
        populateRequestTable();
    }//GEN-LAST:event_processBtnActionPerformed

    private void freshBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_freshBtnActionPerformed
        // TODO add your handling code here:
        populateRequestTable();
    }//GEN-LAST:event_freshBtnActionPerformed

    private void sendMail(String from,String to,String username,String password,String medicineName,int number,String pharmacyName) {
//        String from = "656631832@qq.com";
//        String to = "lx656631832@gmail.com";
//        String username ="656631832";
//        String password = "metkzaiuptzibdeg";
        Properties pro = new Properties();
        
        pro.setProperty("mail.smtp.auth", "true");
        pro.setProperty("mail.smtp.starttls.enable", "true");
        pro.setProperty("mail.smtp.ssl.enable", "true");
        if((from.contains("@gmail.com"))||(from.contains("@husky.neu.edu.com"))){
            pro.setProperty("mail.smtp.ssl.trust", "smtp.gmail.com");
            pro.setProperty("mail.smtp.host", "smtp.gmail.com");
            pro.setProperty("mail.smtp.port", "587");
        }else if(from.contains("@qq.com")){
            pro.setProperty("mail.smtp.host", "smtp.qq.com");
            pro.setProperty("mail.smtp.port", "25");
        }else if(from.contains("@163.com")){
            pro.setProperty("mail.smtp.host", "smtp.163.com");
            pro.setProperty("mail.smtp.port", "25");
        }

        Authenticator aut = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication(){
                return new PasswordAuthentication(username,password);
            }
        };
        Session session = Session.getInstance(pro,aut);
        try{
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject("Comfirmation mail from "+pharmacyName);
            message.setContent(number+" "+medicineName+" have been sent to you,please note to check","text/html;charset=utf-8");
            Transport.send(message);
            System.out.println("send mail successfully");
            JOptionPane.showMessageDialog(null, "You have informed the Medicine Management Department successfully!");
            
        }catch(MessagingException e){
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "the password is wrong, please input again");
            return;
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton freshBtn;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable listTable;
    private javax.swing.JButton processBtn;
    // End of variables declaration//GEN-END:variables
}

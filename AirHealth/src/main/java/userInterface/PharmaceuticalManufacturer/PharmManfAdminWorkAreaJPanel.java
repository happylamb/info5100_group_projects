/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.PharmaceuticalManufacturer;

import business.AirDoctorVolunteerPlatform;
import business.ConfigureSystem;
import business.enterprise.Enterprise;
import business.network.Network;
import business.organization.Organization;
import business.organization.pharmacy.CharityOrganization;
import business.organization.pharmacy.PharmacyAdminOrganization;
import business.role.pharmacy.CharityRole;
import business.user.User;
import business.useraccount.UserAccount;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author gaomc
 */
public class PharmManfAdminWorkAreaJPanel extends javax.swing.JPanel {

    /**
     * Creates new form PharmaceuticalManufacturerAdminWorkAreaJPanel
     */
    private JPanel userProcessContainer;
    private CharityOrganization organization;
    private Enterprise enterprise;
    private AirDoctorVolunteerPlatform system;
    private UserAccount userAccount;
    private Network network;
    public PharmManfAdminWorkAreaJPanel(JPanel userProcessContainer, UserAccount account, PharmacyAdminOrganization organization, Enterprise enterprise) {
        initComponents();

        
    }

    PharmManfAdminWorkAreaJPanel(UserAccount userAccount) {
        initComponents();
        this.network = userAccount.getNetwork();
        this.userAccount = userAccount;
        this.enterprise = userAccount.getEnterprise();
        this.network = userAccount.getNetwork();

        mailCbx.setModel(new DefaultComboBoxModel(new String[]{"@gmail.com","@qq.com","@husky.neu.edu.com","@163.com"}));      

        populateTable();
    }
    
    public void populateTable(){
        DefaultTableModel model = (DefaultTableModel)listTable.getModel();
        
        model.setRowCount(0);

        for(UserAccount useraccount: this.enterprise.getOrganizationDirectory().getOrganizationList()
                .get(1).getUserAccountDirectory().getUserAccountList()){
            Object[] row= new Object[4];
            row[0] = useraccount.getUser().getName();
            row[1] = useraccount;
            row[2] = useraccount.getPassword();
            row[3] = useraccount.getUser().getEmail();
            model.addRow(row);
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        listTable = new javax.swing.JTable();
        addBtn = new javax.swing.JButton();
        nameTxt = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        Password = new javax.swing.JLabel();
        accountTxt = new javax.swing.JTextField();
        pwdTxt = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        mailTxt = new javax.swing.JTextField();
        mailCbx = new javax.swing.JComboBox<>();
        deleteBtn = new javax.swing.JButton();
        jPanel14 = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        listTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Pharmacy name", "Pharmacy accout", "password", "mail address"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(listTable);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 40, 560, 150));

        addBtn.setText("Add");
        addBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addBtnActionPerformed(evt);
            }
        });
        add(addBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 350, -1, -1));

        nameTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nameTxtActionPerformed(evt);
            }
        });
        add(nameTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 210, 100, -1));

        jLabel1.setText("New Pharmacy name");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 210, -1, -1));

        jLabel2.setText("User Account");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 250, -1, -1));

        Password.setText("Password");
        add(Password, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 300, -1, -1));
        add(accountTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 250, 100, -1));
        add(pwdTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 290, 100, -1));

        jLabel3.setText("Mail Address");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 340, -1, -1));

        mailTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mailTxtActionPerformed(evt);
            }
        });
        add(mailTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 330, 100, -1));

        mailCbx.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        mailCbx.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mailCbxActionPerformed(evt);
            }
        });
        add(mailCbx, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 330, 110, -1));

        deleteBtn.setText("delete");
        deleteBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteBtnActionPerformed(evt);
            }
        });
        add(deleteBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 210, -1, -1));

        jPanel14.setBackground(new java.awt.Color(120, 168, 251));

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 91, Short.MAX_VALUE)
        );

        add(jPanel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        jPanel13.setBackground(new java.awt.Color(84, 127, 206));

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );

        add(jPanel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void addBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addBtnActionPerformed
        // TODO add your handling code here:      
        String userName = nameTxt.getText();
        String account = accountTxt.getText();
        String password = pwdTxt.getText();
        String mailAddress = mailTxt.getText()+(String)mailCbx.getSelectedItem();
        User user = new User("Charity");
        user.setName(userName);
        user.setEmail(mailAddress);
        
        CharityRole role = new CharityRole();
        this.enterprise.getOrganizationDirectory().getOrganizationList().get(1).getUserAccountDirectory().createUserAccount(account, password, user, role, network);
        this.enterprise.getOrganizationDirectory().getOrganizationList().get(1).getUserDirectory().addUser(user);

        populateTable();
    }//GEN-LAST:event_addBtnActionPerformed

    private void nameTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nameTxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nameTxtActionPerformed

    private void mailTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mailTxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mailTxtActionPerformed

    private void mailCbxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mailCbxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mailCbxActionPerformed

    private void deleteBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteBtnActionPerformed
        // TODO add your handling code here:
        int selectedrow = listTable.getSelectedRow();
        if(selectedrow >= 0){
            
            UserAccount useraccount = (UserAccount) listTable.getValueAt(selectedrow,1);
            User user = useraccount.getUser();
            
            this.enterprise.getOrganizationDirectory().getOrganizationList().get(1).getUserAccountDirectory().getUserAccountList().remove(useraccount);
            this.enterprise.getOrganizationDirectory().getOrganizationList().get(1).getUserDirectory().getUserList().remove(user);
            
            JOptionPane.showMessageDialog(null, "delete pharmacy user successfully");
            populateTable();
        }else{
            JOptionPane.showMessageDialog(null, "please select a row from list first","Warning",JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_deleteBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Password;
    private javax.swing.JTextField accountTxt;
    private javax.swing.JButton addBtn;
    private javax.swing.JButton deleteBtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable listTable;
    private javax.swing.JComboBox<String> mailCbx;
    private javax.swing.JTextField mailTxt;
    private javax.swing.JTextField nameTxt;
    private javax.swing.JTextField pwdTxt;
    // End of variables declaration//GEN-END:variables
}

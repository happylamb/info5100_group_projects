/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.PharmaceuticalManufacturer;



import business.AirDoctorVolunteerPlatform;
import business.ConfigureSystem;
import business.organization.pharmacy.CharityOrganization;
import business.useraccount.UserAccount;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

/**
 *
 * @author gaomc
 */
public class PharmManfAdminButtonJPanel extends javax.swing.JPanel {
    private JSplitPane mainJSplitPane;
    private UserAccount userAccount;
    private AirDoctorVolunteerPlatform system;
    private CharityOrganization organization;
    /**
     * Creates new form PharmManfAdminButtonJPanel
     */
    public PharmManfAdminButtonJPanel(JSplitPane mainJSplitPane,UserAccount userAccount) {
        initComponents();
        
        this.mainJSplitPane = mainJSplitPane;
        this.userAccount = userAccount;
        this.system = AirDoctorVolunteerPlatform.getInstance();
        nameLbl.setText(this.userAccount.getUser().getName());              
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        side_pane2 = new javax.swing.JPanel();
        manageBtn = new javax.swing.JPanel();
        manageInd = new javax.swing.JPanel();
        pharmacyMngBtn = new javax.swing.JLabel();
        logoutBtn = new javax.swing.JPanel();
        logoutInd = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        nameLbl = new javax.swing.JLabel();
        accountBtn = new javax.swing.JPanel();
        accountInd = new javax.swing.JPanel();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        side_pane2.setBackground(new java.awt.Color(23, 35, 51));
        side_pane2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        manageBtn.setBackground(new java.awt.Color(23, 35, 51));
        manageBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                manageBtnMousePressed(evt);
            }
        });

        manageInd.setOpaque(false);
        manageInd.setPreferredSize(new java.awt.Dimension(3, 43));

        javax.swing.GroupLayout manageIndLayout = new javax.swing.GroupLayout(manageInd);
        manageInd.setLayout(manageIndLayout);
        manageIndLayout.setHorizontalGroup(
            manageIndLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 3, Short.MAX_VALUE)
        );
        manageIndLayout.setVerticalGroup(
            manageIndLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 43, Short.MAX_VALUE)
        );

        pharmacyMngBtn.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        pharmacyMngBtn.setForeground(new java.awt.Color(255, 255, 255));
        pharmacyMngBtn.setText("Manage Pharmacy");
        pharmacyMngBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                pharmacyMngBtnMousePressed(evt);
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pharmacyMngBtnMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout manageBtnLayout = new javax.swing.GroupLayout(manageBtn);
        manageBtn.setLayout(manageBtnLayout);
        manageBtnLayout.setHorizontalGroup(
            manageBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(manageBtnLayout.createSequentialGroup()
                .addComponent(manageInd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pharmacyMngBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE)
                .addContainerGap())
        );
        manageBtnLayout.setVerticalGroup(
            manageBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(manageBtnLayout.createSequentialGroup()
                .addGroup(manageBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(pharmacyMngBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(manageInd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        side_pane2.add(manageBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 100, 140, -1));

        logoutBtn.setBackground(new java.awt.Color(23, 35, 51));
        logoutBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                logoutBtnMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                logoutBtnMouseReleased(evt);
            }
        });

        logoutInd.setOpaque(false);
        logoutInd.setPreferredSize(new java.awt.Dimension(3, 43));

        javax.swing.GroupLayout logoutIndLayout = new javax.swing.GroupLayout(logoutInd);
        logoutInd.setLayout(logoutIndLayout);
        logoutIndLayout.setHorizontalGroup(
            logoutIndLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 3, Short.MAX_VALUE)
        );
        logoutIndLayout.setVerticalGroup(
            logoutIndLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 43, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout logoutBtnLayout = new javax.swing.GroupLayout(logoutBtn);
        logoutBtn.setLayout(logoutBtnLayout);
        logoutBtnLayout.setHorizontalGroup(
            logoutBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(logoutBtnLayout.createSequentialGroup()
                .addComponent(logoutInd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 117, Short.MAX_VALUE))
        );
        logoutBtnLayout.setVerticalGroup(
            logoutBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(logoutBtnLayout.createSequentialGroup()
                .addComponent(logoutInd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        side_pane2.add(logoutBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 220, 120, -1));

        jLabel1.setFont(new java.awt.Font("PilGi", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Welcome!");
        side_pane2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        nameLbl.setForeground(new java.awt.Color(255, 255, 255));
        nameLbl.setText("jLabel2");
        side_pane2.add(nameLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 20, -1, -1));

        accountBtn.setBackground(new java.awt.Color(23, 35, 51));
        accountBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                accountBtnMousePressed(evt);
            }
        });

        accountInd.setOpaque(false);
        accountInd.setPreferredSize(new java.awt.Dimension(3, 43));

        javax.swing.GroupLayout accountIndLayout = new javax.swing.GroupLayout(accountInd);
        accountInd.setLayout(accountIndLayout);
        accountIndLayout.setHorizontalGroup(
            accountIndLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 3, Short.MAX_VALUE)
        );
        accountIndLayout.setVerticalGroup(
            accountIndLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 43, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout accountBtnLayout = new javax.swing.GroupLayout(accountBtn);
        accountBtn.setLayout(accountBtnLayout);
        accountBtnLayout.setHorizontalGroup(
            accountBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(accountBtnLayout.createSequentialGroup()
                .addComponent(accountInd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(117, Short.MAX_VALUE))
        );
        accountBtnLayout.setVerticalGroup(
            accountBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(accountBtnLayout.createSequentialGroup()
                .addComponent(accountInd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        side_pane2.add(accountBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 170, 120, 40));

        add(side_pane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 180, 500));
    }// </editor-fold>//GEN-END:initComponents

    private void pharmacyMngBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pharmacyMngBtnMouseClicked

    }//GEN-LAST:event_pharmacyMngBtnMouseClicked

    private void manageBtnMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_manageBtnMousePressed
        // TODO add your handling code here:
     
        setColor(manageBtn);
        manageInd.setOpaque(true);
        resetColor(new JPanel[]{accountBtn,logoutBtn}, new JPanel[]{accountInd,logoutInd});
        PharmManfAdminWorkAreaJPanel panel = new PharmManfAdminWorkAreaJPanel(userAccount);
        mainJSplitPane.setRightComponent(panel);
    }//GEN-LAST:event_manageBtnMousePressed

    private void logoutBtnMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoutBtnMouseReleased
        // TODO add your handling code here:

      
    }//GEN-LAST:event_logoutBtnMouseReleased

    private void accountBtnMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_accountBtnMousePressed
        // TODO add your handling code here:
//        setColor(accountBtn);
//        accountInd.setOpaque(true);
//        resetColor(new JPanel[]{manageBtn,logoutBtn}, new JPanel[]{manageBtn,logoutInd});
//        PharmManfAdminAccountJPanel panel = new PharmManfAdminAccountJPanel(userAccount,organization);
//        mainJSplitPane.setRightComponent(panel);
    }//GEN-LAST:event_accountBtnMousePressed

    private void pharmacyMngBtnMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pharmacyMngBtnMousePressed
        // TODO add your handling code here:
        PharmManfAdminWorkAreaJPanel panel = new PharmManfAdminWorkAreaJPanel(userAccount);
        mainJSplitPane.setRightComponent(panel);
    }//GEN-LAST:event_pharmacyMngBtnMousePressed

    private void logoutBtnMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoutBtnMousePressed
        // TODO add your handling code here:
        setColor(logoutBtn);
        logoutInd.setOpaque(true);
        resetColor(new JPanel[]{manageBtn,accountBtn}, new JPanel[]{manageInd,accountInd});
    }//GEN-LAST:event_logoutBtnMousePressed

    private void setColor(JPanel pane)
    {
        pane.setBackground(new Color(41,57,80));
    }
    
    private void resetColor(JPanel [] pane, JPanel [] indicators)
    {
        for(int i=0;i<pane.length;i++){
           pane[i].setBackground(new Color(23,35,51));
           
        } for(int i=0;i<indicators.length;i++){
           indicators[i].setOpaque(false);           
        }
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel accountBtn;
    private javax.swing.JPanel accountInd;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel logoutBtn;
    private javax.swing.JPanel logoutInd;
    private javax.swing.JPanel manageBtn;
    private javax.swing.JPanel manageInd;
    private javax.swing.JLabel nameLbl;
    private javax.swing.JLabel pharmacyMngBtn;
    private javax.swing.JPanel side_pane2;
    // End of variables declaration//GEN-END:variables
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.patients;

import business.useraccount.UserAccount;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

/**
 *
 * @author gaomc
 */
public class PatientsButtonJPanel extends javax.swing.JPanel {

    /**
     * Creates new form PatientsButtonJPanel
     */
    JSplitPane splitPane;
    UserAccount userAccount;
    public PatientsButtonJPanel(UserAccount userAccount, JSplitPane splitPane) {
        initComponents();
        this.splitPane = splitPane;
        this.userAccount = userAccount;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        side_pane = new javax.swing.JPanel();
        btn_Home = new javax.swing.JPanel();
        ind_Home = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        btn_AskQuestion = new javax.swing.JPanel();
        ind_2 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        btn_QuestionHistory = new javax.swing.JPanel();
        ind_3 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        btn_donate1 = new javax.swing.JPanel();
        ind_7 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(160, 520));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        side_pane.setBackground(new java.awt.Color(23, 35, 51));
        side_pane.setMinimumSize(new java.awt.Dimension(150, 270));
        side_pane.setPreferredSize(new java.awt.Dimension(150, 313));
        side_pane.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btn_Home.setBackground(new java.awt.Color(23, 35, 51));
        btn_Home.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btn_HomeMousePressed(evt);
            }
        });

        ind_Home.setOpaque(false);
        ind_Home.setPreferredSize(new java.awt.Dimension(3, 43));

        javax.swing.GroupLayout ind_HomeLayout = new javax.swing.GroupLayout(ind_Home);
        ind_Home.setLayout(ind_HomeLayout);
        ind_HomeLayout.setHorizontalGroup(
            ind_HomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 3, Short.MAX_VALUE)
        );
        ind_HomeLayout.setVerticalGroup(
            ind_HomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 43, Short.MAX_VALUE)
        );

        jLabel8.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Home");

        javax.swing.GroupLayout btn_HomeLayout = new javax.swing.GroupLayout(btn_Home);
        btn_Home.setLayout(btn_HomeLayout);
        btn_HomeLayout.setHorizontalGroup(
            btn_HomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btn_HomeLayout.createSequentialGroup()
                .addComponent(ind_Home, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel8)
                .addGap(0, 105, Short.MAX_VALUE))
        );
        btn_HomeLayout.setVerticalGroup(
            btn_HomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btn_HomeLayout.createSequentialGroup()
                .addComponent(ind_Home, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(btn_HomeLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        side_pane.add(btn_Home, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 100, 160, -1));

        btn_AskQuestion.setBackground(new java.awt.Color(23, 35, 51));
        btn_AskQuestion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btn_AskQuestionMousePressed(evt);
            }
        });

        ind_2.setOpaque(false);
        ind_2.setPreferredSize(new java.awt.Dimension(3, 43));

        javax.swing.GroupLayout ind_2Layout = new javax.swing.GroupLayout(ind_2);
        ind_2.setLayout(ind_2Layout);
        ind_2Layout.setHorizontalGroup(
            ind_2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 3, Short.MAX_VALUE)
        );
        ind_2Layout.setVerticalGroup(
            ind_2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 43, Short.MAX_VALUE)
        );

        jLabel10.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Ask a question");

        javax.swing.GroupLayout btn_AskQuestionLayout = new javax.swing.GroupLayout(btn_AskQuestion);
        btn_AskQuestion.setLayout(btn_AskQuestionLayout);
        btn_AskQuestionLayout.setHorizontalGroup(
            btn_AskQuestionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btn_AskQuestionLayout.createSequentialGroup()
                .addComponent(ind_2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel10)
                .addGap(0, 54, Short.MAX_VALUE))
        );
        btn_AskQuestionLayout.setVerticalGroup(
            btn_AskQuestionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btn_AskQuestionLayout.createSequentialGroup()
                .addComponent(ind_2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(btn_AskQuestionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        side_pane.add(btn_AskQuestion, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 140, 160, -1));

        btn_QuestionHistory.setBackground(new java.awt.Color(23, 35, 51));
        btn_QuestionHistory.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btn_QuestionHistoryMousePressed(evt);
            }
        });

        ind_3.setOpaque(false);
        ind_3.setPreferredSize(new java.awt.Dimension(3, 43));

        javax.swing.GroupLayout ind_3Layout = new javax.swing.GroupLayout(ind_3);
        ind_3.setLayout(ind_3Layout);
        ind_3Layout.setHorizontalGroup(
            ind_3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 3, Short.MAX_VALUE)
        );
        ind_3Layout.setVerticalGroup(
            ind_3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 43, Short.MAX_VALUE)
        );

        jLabel11.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Question History");

        javax.swing.GroupLayout btn_QuestionHistoryLayout = new javax.swing.GroupLayout(btn_QuestionHistory);
        btn_QuestionHistory.setLayout(btn_QuestionHistoryLayout);
        btn_QuestionHistoryLayout.setHorizontalGroup(
            btn_QuestionHistoryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btn_QuestionHistoryLayout.createSequentialGroup()
                .addComponent(ind_3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel11)
                .addGap(0, 45, Short.MAX_VALUE))
        );
        btn_QuestionHistoryLayout.setVerticalGroup(
            btn_QuestionHistoryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btn_QuestionHistoryLayout.createSequentialGroup()
                .addGroup(btn_QuestionHistoryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ind_3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(btn_QuestionHistoryLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(55, 55, 55))
        );

        side_pane.add(btn_QuestionHistory, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 180, 160, 50));

        btn_donate1.setBackground(new java.awt.Color(23, 35, 51));
        btn_donate1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btn_donate1MouseReleased(evt);
            }
        });

        ind_7.setOpaque(false);
        ind_7.setPreferredSize(new java.awt.Dimension(3, 43));

        javax.swing.GroupLayout ind_7Layout = new javax.swing.GroupLayout(ind_7);
        ind_7.setLayout(ind_7Layout);
        ind_7Layout.setHorizontalGroup(
            ind_7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 3, Short.MAX_VALUE)
        );
        ind_7Layout.setVerticalGroup(
            ind_7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 43, Short.MAX_VALUE)
        );

        jLabel13.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Donation");

        javax.swing.GroupLayout btn_donate1Layout = new javax.swing.GroupLayout(btn_donate1);
        btn_donate1.setLayout(btn_donate1Layout);
        btn_donate1Layout.setHorizontalGroup(
            btn_donate1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btn_donate1Layout.createSequentialGroup()
                .addComponent(ind_7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel13)
                .addGap(0, 88, Short.MAX_VALUE))
        );
        btn_donate1Layout.setVerticalGroup(
            btn_donate1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btn_donate1Layout.createSequentialGroup()
                .addComponent(ind_7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(btn_donate1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        side_pane.add(btn_donate1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 360, 160, -1));

        add(side_pane, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 160, 540));
    }// </editor-fold>//GEN-END:initComponents

    private void btn_HomeMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_HomeMousePressed
        // TODO add your handling code here:
        setColor(btn_Home);
        ind_Home.setOpaque(true);
        resetColor(new JPanel[]{btn_AskQuestion,btn_QuestionHistory,btn_donate1}, 
                new JPanel[]{ind_2,ind_3,ind_7});
        
        PatientsWorkAreaJPanel patientsWorkArea = new PatientsWorkAreaJPanel(userAccount,splitPane);
        splitPane.setRightComponent(patientsWorkArea);
        
        
    }//GEN-LAST:event_btn_HomeMousePressed

    private void btn_AskQuestionMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_AskQuestionMousePressed
        // TODO add your handling code here:
        setColor(btn_AskQuestion);
        ind_2.setOpaque(true);
        resetColor(new JPanel[]{btn_Home,btn_QuestionHistory,btn_donate1}, 
                new JPanel[]{ind_Home,ind_3,ind_7});
        
        Patients1AskQuestionJPanel askQuestionJPanel 
                = new Patients1AskQuestionJPanel(this.userAccount,splitPane);
        splitPane.setRightComponent(askQuestionJPanel);
    }//GEN-LAST:event_btn_AskQuestionMousePressed

    private void btn_QuestionHistoryMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_QuestionHistoryMousePressed
        // TODO add your handling code here:
        setColor(btn_QuestionHistory);
        ind_3.setOpaque(true);
        resetColor(new JPanel[]{btn_AskQuestion,btn_Home,btn_donate1}, 
                new JPanel[]{ind_Home,ind_2,ind_7});
        
        Patients2QuestionHistoryJPanel questionHistoryJPanel 
                = new Patients2QuestionHistoryJPanel(this.userAccount,splitPane);
        splitPane.setRightComponent(questionHistoryJPanel);

    }//GEN-LAST:event_btn_QuestionHistoryMousePressed

    private void btn_donate1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_donate1MouseReleased
        // TODO add your handling code here:
        setColor(btn_donate1);
        ind_7.setOpaque(true);
        resetColor(new JPanel[]{btn_AskQuestion,btn_Home,btn_QuestionHistory}, 
                new JPanel[]{ind_Home,ind_2,ind_3});
        
        Patients6DonationJPanel donationJPanel = new Patients6DonationJPanel(userAccount, this.splitPane);
        splitPane.setRightComponent(donationJPanel);
    }//GEN-LAST:event_btn_donate1MouseReleased

    private void setColor(JPanel pane)
    {
        pane.setBackground(new Color(41,57,80));
    }
    
    private void resetColor(JPanel [] pane, JPanel [] indicators)
    {
        for(int i=0;i<pane.length;i++){
           pane[i].setBackground(new Color(23,35,51));
           
        } for(int i=0;i<indicators.length;i++){
           indicators[i].setOpaque(false);           
        }
        
    }
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel btn_AskQuestion;
    private javax.swing.JPanel btn_Home;
    private javax.swing.JPanel btn_QuestionHistory;
    private javax.swing.JPanel btn_donate1;
    private javax.swing.JPanel ind_2;
    private javax.swing.JPanel ind_3;
    private javax.swing.JPanel ind_7;
    private javax.swing.JPanel ind_Home;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel side_pane;
    // End of variables declaration//GEN-END:variables
}

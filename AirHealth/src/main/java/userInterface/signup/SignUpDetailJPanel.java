/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.signup;

import business.AirDoctorVolunteerPlatform;
import business.network.Network;
import business.role.Role;
import static business.role.Role.RoleType.Patient;
import business.role.government.VolunteerFinanceRole;
import business.role.patients.PatientsRole;
import business.role.pharmacy.CharityRole;
import business.role.volunteer.FinanceRole;
import business.role.volunteer.MedicineManageRole;
import business.role.volunteer.VolunteerDoctorRole;
import business.user.User;
import business.user.UserDirectory;
import business.user.doctor.Doctor;
import business.user.patients.Patients;
import business.useraccount.UserAccount;
import business.useraccount.UserAccountDirectory;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JSplitPane;

/**
 *
 * @author gaomc
 */
public class SignUpDetailJPanel extends javax.swing.JPanel {

    /**
     * Creates new form SignUpJPanel
     */
    
    private AirDoctorVolunteerPlatform system = AirDoctorVolunteerPlatform.getInstance();
    private User user;
    private Network network;
    private JSplitPane splitPane;
    final String[] departments = {"Others", "Primary Care", "Detist", "Dermatologist", "Optometrist", 
                                "OB_GYN", "Allergist", "Cardiologist", "Ear, Nose&Throat", "Hearing",
                                "Neurologist", "Urgent Care"};
    final String[] income = {"Below 10,000","10,000 - 20,000", "20,000 - 30,000", "30,000 - 50,000", "Above 50,000"};
    
    SignUpDetailJPanel(User user, Network network,JSplitPane splitPane) {
        
        initComponents();
        this.user = user;
        this.network = network;
        this.splitPane = splitPane;
        System.out.println("Sign up user type: " + user.getUserType());
        mailCbx.setModel(new DefaultComboBoxModel(new String[]{"@gmail.com","@qq.com","@husky.neu.edu.com","@163.com"}));      

        
        pnl_p.hide();
        showPanel();
    }
    
    private void showPanel(){
        if(user.getUserType().equals(Role.RoleType.VolunteerDoctor.getValue())){
            lbl_deffrence1.setText("Department: ");
            lbl_difference2.setText("Qulification #:");
            cb_deference1.setModel(new DefaultComboBoxModel(departments)); 
            pnl_p.show();
        }else if(user.getUserType().equals(Role.RoleType.Patient.getValue())){
            lbl_deffrence1.setText("AnnualIncome: ");
            lbl_difference2.setText("MedicalHistory:");
            cb_deference1.setModel(new DefaultComboBoxModel(income));
            pnl_p.show();
        }
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel14 = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        textField_name = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        btn_cancel = new javax.swing.JButton();
        btn_submit = new javax.swing.JButton();
        textField_UserName = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        textField_pwd = new javax.swing.JPasswordField();
        textField_email = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        pnl_p = new javax.swing.JPanel();
        cb_deference1 = new javax.swing.JComboBox<>();
        lbl_deffrence1 = new javax.swing.JLabel();
        textField_difference2 = new javax.swing.JTextField();
        lbl_difference2 = new javax.swing.JLabel();
        mailCbx = new javax.swing.JComboBox<>();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel14.setBackground(new java.awt.Color(120, 168, 251));

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 91, Short.MAX_VALUE)
        );

        add(jPanel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 40, -1));

        jPanel13.setBackground(new java.awt.Color(84, 127, 206));

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );

        add(jPanel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 100, 40));

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 0, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 51, 51));
        jLabel1.setText("Sign Up");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 50, -1, -1));
        add(textField_name, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 150, 220, 40));

        jLabel3.setText("Name: ");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 160, -1, -1));

        btn_cancel.setBackground(new java.awt.Color(255, 255, 255));
        btn_cancel.setText("Cancel");
        btn_cancel.setMaximumSize(new java.awt.Dimension(149, 29));
        btn_cancel.setMinimumSize(new java.awt.Dimension(149, 29));
        btn_cancel.setPreferredSize(new java.awt.Dimension(149, 29));
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });
        add(btn_cancel, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 420, -1, -1));

        btn_submit.setBackground(new java.awt.Color(255, 255, 255));
        btn_submit.setText("Submit ");
        btn_submit.setMaximumSize(new java.awt.Dimension(149, 29));
        btn_submit.setMinimumSize(new java.awt.Dimension(149, 29));
        btn_submit.setPreferredSize(new java.awt.Dimension(149, 29));
        btn_submit.setVerifyInputWhenFocusTarget(false);
        btn_submit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_submitActionPerformed(evt);
            }
        });
        add(btn_submit, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 390, -1, -1));

        textField_UserName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textField_UserNameActionPerformed(evt);
            }
        });
        add(textField_UserName, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 200, 220, 40));

        jLabel4.setText("UserName:");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 210, -1, -1));

        jLabel5.setText("Password:");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 260, -1, -1));

        textField_pwd.setText("jPasswordField1");
        add(textField_pwd, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 250, 220, 40));

        textField_email.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textField_emailActionPerformed(evt);
            }
        });
        add(textField_email, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 300, 110, 40));

        jLabel6.setText("Email:");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 310, -1, -1));

        pnl_p.setBackground(new java.awt.Color(255, 255, 255));

        cb_deference1.setBackground(new java.awt.Color(255, 255, 255));
        cb_deference1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Gastrology", "Infectious Disease", "Neurology", "Internal", "Cardiac", "Plasstic", "Neurosugery" }));
        cb_deference1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_deference1ActionPerformed(evt);
            }
        });

        lbl_deffrence1.setText("Depar/ income");

        textField_difference2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textField_difference2ActionPerformed(evt);
            }
        });

        lbl_difference2.setText("Qual# / medical");

        javax.swing.GroupLayout pnl_pLayout = new javax.swing.GroupLayout(pnl_p);
        pnl_p.setLayout(pnl_pLayout);
        pnl_pLayout.setHorizontalGroup(
            pnl_pLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_pLayout.createSequentialGroup()
                .addGap(0, 110, Short.MAX_VALUE)
                .addGroup(pnl_pLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textField_difference2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_deference1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGroup(pnl_pLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnl_pLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addGroup(pnl_pLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(lbl_deffrence1)
                        .addComponent(lbl_difference2))
                    .addGap(0, 229, Short.MAX_VALUE)))
        );
        pnl_pLayout.setVerticalGroup(
            pnl_pLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_pLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cb_deference1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                .addComponent(textField_difference2, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15))
            .addGroup(pnl_pLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnl_pLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(lbl_deffrence1)
                    .addGap(34, 34, 34)
                    .addComponent(lbl_difference2)
                    .addGap(0, 43, Short.MAX_VALUE)))
        );

        add(pnl_p, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 350, 330, 110));

        mailCbx.setBackground(new java.awt.Color(255, 255, 255));
        mailCbx.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        mailCbx.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mailCbxActionPerformed(evt);
            }
        });
        add(mailCbx, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 300, 110, 40));
    }// </editor-fold>//GEN-END:initComponents

    private void cb_deference1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_deference1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb_deference1ActionPerformed

    private void textField_difference2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textField_difference2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textField_difference2ActionPerformed

    private void btn_submitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_submitActionPerformed
        // TODO add your handling code here:
        String name = textField_name.getText();
        String username = textField_UserName.getText();
        String password = textField_pwd.getText();
        String type = user.getUserType();
        String defference1 = (String) cb_deference1.getSelectedItem();
        String defference2 = textField_difference2.getText();
        
        if(!type.equals(Role.RoleType.Patient.getValue())
                    && !type.equals(Role.RoleType.VolunteerDoctor.getValue())){
            textField_difference2.setText(".");
        }
        if(usernamePatternCorrect()
                && !textField_name.getText().equals("") 
                && !textField_UserName.getText().equals("")
                && !textField_pwd.getText().equals("")
                && !textField_difference2.getText().equals("")){
            
            String email = textField_email.getText()+(String)mailCbx.getSelectedItem();
            user.setName(name);
            user.setEmail(email);
            
            if(type.equals(Role.RoleType.Patient.getValue())){
                
                Patients patient = (Patients) user;
                patient.setMedicalHistory(defference2);
                patient.setIncome(defference1);
                UserAccountDirectory uad =  network.getEnterpriseDirectory().getEnterpriseList().get(0)
                        .getOrganizationDirectory().getOrganizationList().get(4)
                        .getUserAccountDirectory();
                if(uad.checkIfUsernameIsUnique(username)){
                    UserAccount ua = uad.createUserAccount(username, password, patient, new PatientsRole(),network);
                    System.out.println("patientsList: " + uad.getUserAccountList());
                    ua.setEnterprise(network.getEnterpriseDirectory().getEnterpriseList().get(0));
                    JOptionPane.showMessageDialog(null, "Congratulations! user " + user.getName() + " add to "
                                        + user.getUserType() + " Organization. ");
                }else{
                    JOptionPane.showMessageDialog(null, "This user account name has been used! ");
                }  
            }else if(type.equals(Role.RoleType.VolunteerDoctor.getValue())){
                
                Doctor doctor = (Doctor) user;
                // EnterpriseList.get(0) -> Volunteer
                // .OrganizationList.get(1) -> doctor
                doctor.setDepartment(defference1);
                doctor.setQulification_number(textField_difference2.getText());
                UserAccountDirectory uad = network.getEnterpriseDirectory().getEnterpriseList().get(0)
                        .getOrganizationDirectory().getOrganizationList()
                        .get(1).getUserAccountDirectory();
                if(uad.checkIfUsernameIsUnique(username)){
                    UserAccount ua = uad.createUserAccount(username, password, doctor, new VolunteerDoctorRole(),network);
                    ua.setEnterprise(network.getEnterpriseDirectory().getEnterpriseList().get(0));
                    
                    System.out.println("doctorlist: " + uad.getUserAccountList());
                     JOptionPane.showMessageDialog(null, "Congratulations! user " + user.getName() + " add to "
                                        + user.getUserType() + " Organization. ");
                }else{
                    JOptionPane.showMessageDialog(null, "This user account name has been used! ");
                } 

            }else if(type.equals(Role.RoleType.MedicineManage.getValue())){
                UserAccountDirectory uad = network.getEnterpriseDirectory().getEnterpriseList().get(0)
                        .getOrganizationDirectory().getOrganizationList()
                        .get(2).getUserAccountDirectory();
                if(uad.checkIfUsernameIsUnique(username)){
                    UserAccount ua = uad.createUserAccount(username, password, user, new MedicineManageRole(),network);
                    ua.setEnterprise(network.getEnterpriseDirectory().getEnterpriseList().get(0));
                    System.out.println("medicinemanageList: " +  uad.getUserAccountList());
                    JOptionPane.showMessageDialog(null, "Congratulations! user " + user.getName() + " add to "
                                        + user.getUserType() + " Organization. ");
                }else{
                    JOptionPane.showMessageDialog(null, "This user account name has been used! ");
                }  
            }else if(type.equals(Role.RoleType.Finance.getValue())){
                UserAccountDirectory uad = network.getEnterpriseDirectory().getEnterpriseList().get(0)
                        .getOrganizationDirectory().getOrganizationList()
                        .get(3).getUserAccountDirectory();
                if(uad.checkIfUsernameIsUnique(username)){
                    UserAccount ua = uad.createUserAccount(username, password, user, new FinanceRole(),network);
                    ua.setEnterprise(network.getEnterpriseDirectory().getEnterpriseList().get(0));

                    System.out.println("FinanceList: " +  uad.getUserAccountList());
                     JOptionPane.showMessageDialog(null, "Congratulations! user " + user.getName() + " add to "
                                        + user.getUserType() + " Organization. ");
                }else{
                    JOptionPane.showMessageDialog(null, "This user account name has been used! ");
                }  
            }else if(type.equals(Role.RoleType.VolunteerFiance.getValue())){
                UserAccountDirectory uad = network.getEnterpriseDirectory().getEnterpriseList().get(1)
                        .getOrganizationDirectory().getOrganizationList()
                        .get(1).getUserAccountDirectory();
                if(uad.checkIfUsernameIsUnique(username)){
                    UserAccount ua = uad.createUserAccount(username, password, user, new VolunteerFinanceRole(),network);
                    ua.setEnterprise(network.getEnterpriseDirectory().getEnterpriseList().get(1));
                    System.out.println("VolunteerFianceList: " +  uad.getUserAccountList());
                    JOptionPane.showMessageDialog(null, "Congratulations! user " + user.getName() + " add to "
                                        + user.getUserType() + " Organization. ");
                }else{
                    JOptionPane.showMessageDialog(null, "This user account name has been used! ");
                }            
            }else if(type.equals(Role.RoleType.Charity.getValue())){
                UserAccountDirectory uad = network.getEnterpriseDirectory().getEnterpriseList().get(2)
                        .getOrganizationDirectory().getOrganizationList()
                        .get(1).getUserAccountDirectory();
                if(uad.checkIfUsernameIsUnique(username)){
                    UserAccount ua = uad.createUserAccount(username, password, user, new CharityRole(),network);
                    ua.setEnterprise(network.getEnterpriseDirectory().getEnterpriseList().get(2));
                    System.out.println("CharityList: " +  uad.getUserAccountList());
                     JOptionPane.showMessageDialog(null, "Congratulations! user " + user.getName() + " add to "
                                        + user.getUserType() + " Organization. ");
                }else{
                    JOptionPane.showMessageDialog(null, "This user account name has been used! ");
                }  
            }
        }else{
            if(!usernamePatternCorrect()){
                JOptionPane.showMessageDialog(null, "Please check your email address.");
            }else{
                JOptionPane.showMessageDialog(null, "Please don't leave blank!");
            }
        }
        
    }//GEN-LAST:event_btn_submitActionPerformed

    private void textField_UserNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textField_UserNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textField_UserNameActionPerformed

    private void textField_emailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textField_emailActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textField_emailActionPerformed

    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
        // TODO add your handling code here:
        splitPane.setRightComponent(new SignUpTypeJPanel(splitPane));
    }//GEN-LAST:event_btn_cancelActionPerformed

    private void mailCbxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mailCbxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mailCbxActionPerformed
    
    private boolean usernamePatternCorrect() {
        Pattern p = Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");
        String mailAddress = textField_email.getText()+(String)mailCbx.getSelectedItem();
        Matcher m = p.matcher(mailAddress);
        boolean result = m.matches();
        System.out.println("Email Pattern: " + result);
        return result;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancel;
    private javax.swing.JButton btn_submit;
    private javax.swing.JComboBox<String> cb_deference1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JLabel lbl_deffrence1;
    private javax.swing.JLabel lbl_difference2;
    private javax.swing.JComboBox<String> mailCbx;
    private javax.swing.JPanel pnl_p;
    private javax.swing.JTextField textField_UserName;
    private javax.swing.JTextField textField_difference2;
    private javax.swing.JTextField textField_email;
    private javax.swing.JTextField textField_name;
    private javax.swing.JPasswordField textField_pwd;
    // End of variables declaration//GEN-END:variables
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.volunteer.medicineManage;

import business.useraccount.UserAccount;
import business.workqueue.DoctorRequest;
import business.workqueue.MedicineRequest;
import business.workqueue.WorkQueue;
import business.workqueue.WorkRequest;
import javax.swing.JOptionPane;
import javax.swing.JSplitPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author sallywang
 */
public class MedicineContactPharmacyJPanel extends javax.swing.JPanel {
    private UserAccount userAccount;
    private JSplitPane splitPane;
    private WorkQueue workQueue;
    /**
     * Creates new form MedicineContactPharmacyJPanel
     */
    public MedicineContactPharmacyJPanel(UserAccount userAccount,JSplitPane splitPane) {
        initComponents();
        
        this.userAccount = userAccount;
        this.splitPane = splitPane;
        this.workQueue = userAccount.getNetwork().getSepcificWorkQueue("notification_Pharmacy");
        
        
        populateTable1();
        populateTable2();
    }
    public void populateTable1(){
        DefaultTableModel model = (DefaultTableModel) listTable1.getModel();
        model.setRowCount(0);
        
        for(WorkRequest request1: workQueue.getWorkRequestList()){
            MedicineRequest request = (MedicineRequest)request1;
            if(request.getStatus().equals("unhandled")){
                Object[] row = new Object[3];
                row[0] = request.getRequestDate();
                row[1] = request.getMedicineName();
                row[2] = request.getQuantity();


                model.addRow(row);
            }
        }
    }
    public void populateTable2(){
        DefaultTableModel model = (DefaultTableModel) listTable2.getModel();
        model.setRowCount(0);
        
        for(WorkRequest request1: workQueue.getWorkRequestList()){
            MedicineRequest request = (MedicineRequest)request1;
            if(request.getStatus().equals("completed")){
                Object[] row = new Object[5];
                row[0] = request.getRequestDate();
                row[1] = request.getResolveDate();
                row[2] = request.getMedicineName();
                row[3] = request.getQuantity();
                row[4] = request.getReceiver();

                model.addRow(row);
            }
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        listTable2 = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        listTable1 = new javax.swing.JTable();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        nameTxt = new javax.swing.JTextField();
        numberTxt = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jTabbedPane1.setBackground(new java.awt.Color(255, 255, 255));

        listTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "send date", "respond date", "medicine", "amount", "pharmacy"
            }
        ));
        jScrollPane1.setViewportView(listTable2);

        jTabbedPane1.addTab("handled", jScrollPane1);

        listTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "date", "medicine", "amount"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane2.setViewportView(listTable1);

        jTabbedPane1.addTab("unhandled", jScrollPane2);

        add(jTabbedPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(26, 6, 695, 235));

        jButton2.setBackground(new java.awt.Color(255, 255, 255));
        jButton2.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jButton2.setText("send request");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 280, -1, 60));

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel1.setText("Required Medicine Infomation");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 250, -1, -1));

        jLabel2.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel2.setText("- Quantity");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 330, -1, -1));
        add(nameTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 280, 130, 30));
        add(numberTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 320, 130, 30));

        jLabel4.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel4.setText("- Name");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 290, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        String name = nameTxt.getText();
        int number = Integer.parseInt(numberTxt.getText());
        MedicineRequest newRequest = new MedicineRequest();
        newRequest.setMedicineName(name);
        newRequest.setQuantity(number);
        newRequest.setSender(userAccount);
        workQueue.getWorkRequestList().add(newRequest);
        JOptionPane.showMessageDialog(null, "the request has been sent to all pharmacies");
        populateTable1();
        populateTable2();        
    }//GEN-LAST:event_jButton2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable listTable1;
    private javax.swing.JTable listTable2;
    private javax.swing.JTextField nameTxt;
    private javax.swing.JTextField numberTxt;
    // End of variables declaration//GEN-END:variables
}

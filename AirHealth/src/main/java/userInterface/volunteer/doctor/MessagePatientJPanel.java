/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.volunteer.doctor;

import business.workqueue.others.QuestionEntity;
import java.time.format.DateTimeFormatter;
import javax.swing.SwingConstants;

/**
 *
 * @author sallywang
 */
public class MessagePatientJPanel extends javax.swing.JPanel {

    private QuestionEntity qe;
    /**
     * Creates new form PatientMassageJPanel
     */
    public MessagePatientJPanel(QuestionEntity qe) {
        initComponents();
        this.qe = qe;
        this.namejLabel.setText(qe.getName());
//        this.messagejTextArea.setText(qe.getQuestion_answer());
        this.jLabel1.setText(qe.getQuestion_answer());
        this.datetimejLabel.setText(qe.getTime().format(DateTimeFormatter.ISO_DATE_TIME));
        jLabel1.setVerticalTextPosition(SwingConstants.CENTER);
        jLabel1.setHorizontalTextPosition(SwingConstants.CENTER);
        this.namejLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        namejLabel = new javax.swing.JLabel();
        datetimejLabel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        namejLabel.setText("patient name:");
        add(namejLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 50, -1));

        datetimejLabel.setForeground(new java.awt.Color(153, 153, 153));
        datetimejLabel.setText("2019-11-13 18：00");
        add(datetimejLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 50, -1, -1));

        jLabel1.setText("jLabel1");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 10, 480, 40));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/chatLeft.png")));
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel datetimejLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel namejLabel;
    // End of variables declaration//GEN-END:variables
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.volunteer.doctor;

import javax.swing.JPanel;

/**
 *
 * @author sallywang
 */
public class Scroll_BoxJPanel extends javax.swing.JPanel {

    /**
     * Creates new form QuestionsJPanel
     */
    public Scroll_BoxJPanel() {
        initComponents();
//        QuestionItemJPanel qj1 = new QuestionItemJPanel();
//        QuestionItemJPanel qj2 = new QuestionItemJPanel();
////        this.boxLayoutJPanel.add(qj2);
////        this.boxLayoutJPanel.add(qj1);
//        addJPanelItem(qj1);
//        addJPanelItem(qj2);
        
    }
    
    public void addJPanelItem(JPanel item){
        this.boxLayoutJPanel.add(item);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ScrollPanel = new javax.swing.JScrollPane();
        boxLayoutJPanel = new javax.swing.JPanel();

        setBackground(new java.awt.Color(255, 255, 255));

        ScrollPanel.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        boxLayoutJPanel.setBackground(new java.awt.Color(255, 255, 255));
        boxLayoutJPanel.setLayout(new javax.swing.BoxLayout(boxLayoutJPanel, javax.swing.BoxLayout.Y_AXIS));
        ScrollPanel.setViewportView(boxLayoutJPanel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 833, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(ScrollPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 827, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 509, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(ScrollPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 503, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane ScrollPanel;
    private javax.swing.JPanel boxLayoutJPanel;
    // End of variables declaration//GEN-END:variables
}

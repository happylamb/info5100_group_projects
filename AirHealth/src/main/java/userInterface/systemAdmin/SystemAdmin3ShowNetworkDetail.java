/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.systemAdmin;

import business.AirDoctorVolunteerPlatform;
import business.enterprise.Enterprise;
import business.network.Network;
import business.organization.Organization;
import business.useraccount.UserAccount;
import java.util.ArrayList;
import javax.swing.JSplitPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

/**
 *
 * @author gaomc
 */
public class SystemAdmin3ShowNetworkDetail extends javax.swing.JPanel {

    /**
     * Creates new form SystemAdmin3ShowNetworkDetail
     */
    
    private AirDoctorVolunteerPlatform system = AirDoctorVolunteerPlatform.getInstance();

    private Network network;
    private JSplitPane splitPane;
    
    public SystemAdmin3ShowNetworkDetail(Network network, JSplitPane splitPane) {
        initComponents();
        this.network = network;
        this.splitPane = splitPane;
        lbl_networkName.setText(network.toString());
        pnl_ent.hide();
        pnl_org.hide();
        pnl_userAccount.hide();
        pnl_network.hide();
        populateJTree();
    }

    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTree = new javax.swing.JTree();
        jLabel1 = new javax.swing.JLabel();
        lbl_networkName = new javax.swing.JLabel();
        pnl_userAccount = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txt_userAccount = new javax.swing.JLabel();
        txt_userName = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txt_utype = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txt_email = new javax.swing.JLabel();
        pnl_network = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txt_netName = new javax.swing.JLabel();
        btn_checkWorkRequest = new javax.swing.JButton();
        pnl_ent = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        txt_entType = new javax.swing.JLabel();
        txt_orgCount = new javax.swing.JLabel();
        pnl_org = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txt_userList = new javax.swing.JLabel();
        txt_orgType = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txt_intAmount = new javax.swing.JLabel();
        btn_back = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBackground(new java.awt.Color(120, 168, 251));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 91, Short.MAX_VALUE)
        );

        add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 40, -1));

        jPanel3.setBackground(new java.awt.Color(84, 127, 206));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );

        add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 100, 40));

        jTree.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener() {
            public void valueChanged(javax.swing.event.TreeSelectionEvent evt) {
                jTreeValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jTree);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 120, 470, 240));

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 0, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 51, 51));
        jLabel1.setText("Current Network: ");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 60, -1, -1));

        lbl_networkName.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        lbl_networkName.setForeground(new java.awt.Color(0, 153, 204));
        lbl_networkName.setText("_network");
        add(lbl_networkName, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 70, -1, -1));

        pnl_userAccount.setBackground(new java.awt.Color(255, 255, 255));
        pnl_userAccount.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel5.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel5.setText("UserAccount Details: ");
        pnl_userAccount.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        jLabel6.setText("User name: ");
        pnl_userAccount.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, -1, -1));

        jLabel7.setText("UserAccount:  ");
        pnl_userAccount.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, -1, -1));

        txt_userAccount.setText("<UserAccount>");
        pnl_userAccount.add(txt_userAccount, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 90, -1, -1));

        txt_userName.setText("<name>");
        pnl_userAccount.add(txt_userName, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 60, -1, -1));

        jLabel8.setText("Type: ");
        pnl_userAccount.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, -1, -1));

        txt_utype.setText("<Type>");
        pnl_userAccount.add(txt_utype, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 150, -1, -1));

        jLabel13.setText("Email:");
        pnl_userAccount.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, -1, -1));

        txt_email.setText("<Email>");
        pnl_userAccount.add(txt_email, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 120, -1, -1));

        add(pnl_userAccount, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 80, 260, 280));

        pnl_network.setBackground(new java.awt.Color(255, 255, 255));
        pnl_network.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel10.setText("Network name: ");
        pnl_network.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, -1, -1));

        jLabel14.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel14.setText("Network Details: ");
        pnl_network.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        jLabel11.setText("WorkRequest Count:");
        pnl_network.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, -1, -1));

        txt_netName.setText("<name>");
        pnl_network.add(txt_netName, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 90, -1, -1));

        btn_checkWorkRequest.setBackground(new java.awt.Color(255, 255, 255));
        btn_checkWorkRequest.setFont(new java.awt.Font("Lucida Grande", 0, 24)); // NOI18N
        btn_checkWorkRequest.setText("10");
        btn_checkWorkRequest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_checkWorkRequestActionPerformed(evt);
            }
        });
        pnl_network.add(btn_checkWorkRequest, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 160, 160, 110));

        add(pnl_network, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 80, 260, 280));

        pnl_ent.setBackground(new java.awt.Color(255, 255, 255));
        pnl_ent.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel9.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel9.setText("EnterPrise Details: ");
        pnl_ent.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        jLabel16.setText("Organization Count:");
        pnl_ent.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, -1, -1));

        jLabel15.setText("Enterprise Type: ");
        pnl_ent.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, -1, -1));

        txt_entType.setText("<type>");
        pnl_ent.add(txt_entType, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 90, -1, -1));

        txt_orgCount.setText("<orgCount>");
        pnl_ent.add(txt_orgCount, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 150, -1, -1));

        add(pnl_ent, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 80, 260, 280));

        pnl_org.setBackground(new java.awt.Color(255, 255, 255));
        pnl_org.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel2.setText("Organization Details: ");
        pnl_org.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        jLabel3.setText("Organization Type: ");
        pnl_org.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, -1, -1));

        jLabel4.setText("UserAccountList: ");
        pnl_org.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, -1, -1));

        txt_userList.setText("<UserAccount>");
        pnl_org.add(txt_userList, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 150, -1, -1));

        txt_orgType.setText("<type>");
        pnl_org.add(txt_orgType, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 90, -1, -1));

        jLabel12.setText("UserAccount Amount: ");
        pnl_org.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 180, -1, -1));

        txt_intAmount.setText("<int amount>");
        pnl_org.add(txt_intAmount, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 210, -1, -1));

        add(pnl_org, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 80, 260, 280));

        btn_back.setBackground(new java.awt.Color(255, 255, 255));
        btn_back.setText("<< Back");
        btn_back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_backActionPerformed(evt);
            }
        });
        add(btn_back, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 370, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void jTreeValueChanged(javax.swing.event.TreeSelectionEvent evt) {//GEN-FIRST:event_jTreeValueChanged
        
        DefaultMutableTreeNode selectedNode= (DefaultMutableTreeNode)jTree.getLastSelectedPathComponent();
        if(selectedNode!=null){
            jLabel2.setText(selectedNode.toString());
                
            if (selectedNode.getUserObject() instanceof Organization){
                Organization org = (Organization) selectedNode.getUserObject();
                pnl_org.show();
                pnl_ent.hide();
                pnl_userAccount.hide();
                pnl_network.hide();
                txt_orgType.setText("< " +org.toString()+ " >");
                txt_userList.setText("< " + org.getUserAccountDirectory().getUserAccountList().toString() + " >");
                txt_intAmount.setText("< " + Integer.toString(org.getUserAccountDirectory().getUserAccountList().size()) + " >");
            }else if(selectedNode.getUserObject() instanceof Enterprise){
                Enterprise ent= (Enterprise) selectedNode.getUserObject();
                pnl_ent.show();
                pnl_org.hide();
                pnl_userAccount.hide();
                pnl_network.hide();
                txt_entType.setText("< " + ent.toString() + " >");
                txt_orgCount.setText("< " + Integer.toString(ent.getOrganizationDirectory().getOrganizationList().size()) + " >");
            }else if(selectedNode.getUserObject() instanceof UserAccount){
                UserAccount ua = (UserAccount) selectedNode.getUserObject();
                pnl_ent.hide();
                pnl_org.hide();
                pnl_userAccount.show();
                pnl_network.hide();
                txt_userName.setText("< " + ua.getUser().getName() + " >");
                txt_userAccount.setText("< " + ua.getUsername() + " >");
                txt_utype.setText("< " + ua.getRole().toString() + " >");
                txt_email.setText("< " + ua.getUser().getEmail() + " >");
            }else if(selectedNode.getUserObject() instanceof Network){
                Network network = (Network) selectedNode.getUserObject();
                pnl_ent.hide();
                pnl_org.hide();
                pnl_userAccount.hide();
                pnl_network.show();
                txt_netName.setText("< "+ network.getName() + " >");
                btn_checkWorkRequest.setText("< " +Integer.toString(network.getWorkRequestCount()) + " >");
            }

        }
        
    }//GEN-LAST:event_jTreeValueChanged

    private void btn_checkWorkRequestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_checkWorkRequestActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_checkWorkRequestActionPerformed

    private void btn_backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_backActionPerformed
        // TODO add your handling code here:
        splitPane.setRightComponent(new SystemAdmin1ManageNetworkJPanel(splitPane));
    }//GEN-LAST:event_btn_backActionPerformed

    private void populateJTree(){
        DefaultTreeModel model=(DefaultTreeModel)jTree.getModel();
        ArrayList<Enterprise> enterpriseList = this.network.getEnterpriseDirectory().getEnterpriseList();
        ArrayList<Organization> organizationList = new ArrayList<>();
        ArrayList<UserAccount> userAccountList = new ArrayList<>();
        
        Network network;
        Enterprise enterprise;
        Organization organization;
        UserAccount userAccount;
        
        DefaultMutableTreeNode networkRook = new DefaultMutableTreeNode(this.network);
        DefaultMutableTreeNode root=(DefaultMutableTreeNode)model.getRoot();
        root.removeAllChildren();
        root.insert(networkRook, 0);
        
        DefaultMutableTreeNode networkNode;
        DefaultMutableTreeNode enterpriseNode;
        DefaultMutableTreeNode organizationNode;
        DefaultMutableTreeNode userAccountNode;
        
        
        for(int i=0;i < enterpriseList.size();i++){
            enterprise = enterpriseList.get(i);
            enterpriseNode=new DefaultMutableTreeNode(enterprise);
            networkRook.insert(enterpriseNode, i);
            
            organizationList = enterprise.getOrganizationDirectory().getOrganizationList();
            for(int j=0; j<organizationList.size();j++){
                organization = organizationList.get(j);
                organizationNode = new DefaultMutableTreeNode(organization);
                enterpriseNode.insert(organizationNode, j);
                
                userAccountList = organization.getUserAccountDirectory().getUserAccountList();
                for(int k=0;k < userAccountList.size();k++){
                    userAccount = userAccountList.get(k);
                    userAccountNode = new DefaultMutableTreeNode(userAccount);
                    System.out.println("jtree  " + userAccount.toString());
                    organizationNode.insert(userAccountNode, k);
                }
            }
        }
        model.reload();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_back;
    private javax.swing.JButton btn_checkWorkRequest;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTree jTree;
    private javax.swing.JLabel lbl_networkName;
    private javax.swing.JPanel pnl_ent;
    private javax.swing.JPanel pnl_network;
    private javax.swing.JPanel pnl_org;
    private javax.swing.JPanel pnl_userAccount;
    private javax.swing.JLabel txt_email;
    private javax.swing.JLabel txt_entType;
    private javax.swing.JLabel txt_intAmount;
    private javax.swing.JLabel txt_netName;
    private javax.swing.JLabel txt_orgCount;
    private javax.swing.JLabel txt_orgType;
    private javax.swing.JLabel txt_userAccount;
    private javax.swing.JLabel txt_userList;
    private javax.swing.JLabel txt_userName;
    private javax.swing.JLabel txt_utype;
    // End of variables declaration//GEN-END:variables
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import business.enterprise.Enterprise;
import business.enterprise.PharmacyEnterprise;
import business.network.Network;
import business.organization.Organization;
import static business.organization.Organization.Type.Patients;
import static business.organization.Organization.Type.PharmacyAdmin;
import business.organization.pharmacy.CharityOrganization;
import business.organization.pharmacy.PharmacyAdminOrganization;
import business.user.employee.Employee;
import business.role.SystemAdminRole;
import business.role.patients.PatientsRole;
import business.role.pharmacy.CharityRole;
import business.role.pharmacy.PharmacyAdminRole;
import business.role.volunteer.VolunteerDoctorRole;
import business.user.User;
import business.user.UserDirectory;
import business.user.charity.MedicineList;
import business.user.patients.Patients;
import business.useraccount.UserAccount;
import business.useraccount.UserAccountDirectory;
import business.workqueue.MedicineRequest;
import business.workqueue.WorkQueue;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author xinlu
 */
public class ConfigureSystem {
    
        public static AirDoctorVolunteerPlatform configure(AirDoctorVolunteerPlatform system){
        
//        AirDoctorVolunteerPlatform system = AirDoctorVolunteerPlatform.getInstance();
        
        UserAccountDirectory userAccountDirectory = system.getUserAccountDirectory();
        UserDirectory userDirectory = system.getUserDirectory();
        
        //Set WorkQueueInventory
        
        //Create a network
        //create an enterprise
        //initialize some organizations
        //have some employees 
        //create user account
        
        User user = userDirectory.createUser("sys", "SystemAdministration");
        UserAccount ua = userAccountDirectory.createUserAccount("sys", "sys", user, new SystemAdminRole(),null);
        
//        Patients patient1 = new Patients();
//        userDirectory.addUser(patient1);
//        UserAccount userAccount_patient1 = userAccountDirectory.createUserAccount("json", "json", patient1, new PatientsRole());
//           
//        User doctor1 = userDirectory.createUser("mike_doctor", "VolunteerDoctor");
//        UserAccount userAccount_doctor1 = userAccountDirectory.createUserAccount("mike", "mike", doctor1, new VolunteerDoctorRole());
//        
//        PharmacyEnterprise enterprise = new PharmacyEnterprise("CVS");
//        CharityOrganization cOrganization = new CharityOrganization();
//        PharmacyAdminOrganization paOrganizatio = new PharmacyAdminOrganization();
//        enterprise.getOrganizationDirectory().getOrganizationList().add(cOrganization);
//        enterprise.getOrganizationDirectory().getOrganizationList().add(paOrganizatio);
//        
//
//        User pharmacyAdmin = userDirectory.createUser("pharmacy_admin", "pharmacyAdmin");
//        UserAccount userAccount_pharmacyAdmin = userAccountDirectory.createUserAccount("pa", "pa", pharmacyAdmin, new PharmacyAdminRole());
//        userAccount_pharmacyAdmin.setEnterprise(enterprise);

//        WorkQueue patient_doctorQ = system.getSepcificWorkQueue("patient_docQuestion");
//        QuestionWorkRequest qw1 = new QuestionWorkRequest(patient1);
//        qw1.askQuestion("Why my stomachache?");
//        
//        QuestionWorkRequest qw2 = new QuestionWorkRequest(patient1);
//        qw2.askQuestion("Why my headache?");
//        
//        QuestionWorkRequest qw3 = new QuestionWorkRequest(patient1);
//        qw3.askQuestion("Why my legache?");
//        
//        patient_doctorQ.getWorkRequestList().add(qw1);
//        patient_doctorQ.getWorkRequestList().add(qw2);
//        patient_doctorQ.getWorkRequestList().add(qw3);
//        
//        qw1.answerQuestion(doctor1, "because you eat shit");
//        qw1.answerQuestion(doctor1, "because you head is squized by the door");
//        qw1.answerQuestion(doctor1, "I don't know why");
        
        
        
        
        return system;
    }
}

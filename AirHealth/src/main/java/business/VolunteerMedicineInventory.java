/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author sallywang
 */
public class VolunteerMedicineInventory {
    Map<String,Integer> medicineInventory;

    public VolunteerMedicineInventory() {
        this.medicineInventory = new HashMap<>();
    }
    
    /**
     * 
     * @param medicine
     * @param checkout_amount
     * @return remain medicine amount,
     * if remain is positive number, 
     *  then medicineIventory would successfully checkout the medicine, and return the remain amount
     * if the remain is negative number,
     *  then medicineInventory would not checkout, and return the negative number which means we should send a request to pharmacy
     */
    public int checkoutMedicine(String medicine,int checkout_amount){
        int remain = medicineInventory.get(medicine).intValue() - checkout_amount;
        if(remain >= 0){
            medicineInventory.put(medicine, new Integer(remain));
        }
        return remain;
    }

    public Map<String, Integer> getMedicineInventory() {
        return medicineInventory;
    }

    public void setMedicineInventory(Map<String, Integer> medicineInventory) {
        this.medicineInventory = medicineInventory;
    }
    
    /**
     * 
     * @param medicine
     * @param add_amount
     * @return updated medicine amount
     */
    public int addMedicine(String medicine, int add_amount){
        int updated_amount = medicineInventory.get(medicine).intValue() + add_amount;
        medicineInventory.put(medicine, new Integer(updated_amount));
        return updated_amount;
    }  
    
    public Boolean hasMedicine(String medicine){
        return medicineInventory.containsKey(medicine);
    }
    
    public int remainAmount(String medicine){
        return medicineInventory.get(medicine);
    } 
    
}

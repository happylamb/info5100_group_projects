/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.network;

import business.VolunteerMedicineInventory;
import business.enterprise.Enterprise;
import business.enterprise.EnterpriseDirectory;
import business.organization.Organization;
import business.useraccount.UserAccount;
import business.useraccount.UserAccountDirectory;
import business.workqueue.WorkQueue;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 *
 * @author xinlu
 */
public class Network {
    
    private String name;
    private EnterpriseDirectory enterpriseDirectory;
    private HashMap<String,WorkQueue> workQueuesHashMap;
    private VolunteerMedicineInventory volunteerMedInventory;
    
    public Network(){
        enterpriseDirectory=new EnterpriseDirectory();
        volunteerMedInventory = new VolunteerMedicineInventory();
        
        setWorkQueueInventory();
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EnterpriseDirectory getEnterpriseDirectory() {
        return enterpriseDirectory;
    }
    
    public VolunteerMedicineInventory getVolunteerMedicineInventory(){
        return this.volunteerMedInventory;
    }
    
    public int countUsers(){
        int count = 0;
        for(Enterprise e : this.getEnterpriseDirectory().getEnterpriseList()){
            for(Organization o : e.getOrganizationDirectory().getOrganizationList()){
                for(UserAccount u : o.getUserAccountDirectory().getUserAccountList()){
                    count ++;
                }
            }
        }
        return count;
    }
    
    public void setWorkQueueInventory(){
        
        this.workQueuesHashMap = new HashMap<>();
         //add workQueue for Patient
        workQueuesHashMap.put("patient_docQuestion", new WorkQueue());
        
        //add workQueue for Volunteer Organization
        workQueuesHashMap.put("doctor_contactHospital", new WorkQueue());
        workQueuesHashMap.put("contactHospital_clinic", new WorkQueue());
        workQueuesHashMap.put("notification_Patient", new WorkQueue());
        workQueuesHashMap.put("notification_Pharmacy", new WorkQueue());
        workQueuesHashMap.put("Donation", new WorkQueue());
        
        //add workQueue for Gov
        workQueuesHashMap.put("finance_government", new WorkQueue());
        
        workQueuesHashMap.put("doctor_medicineManage", new WorkQueue());
        workQueuesHashMap.put("medicineManage_pharmacy", new WorkQueue());
    }
    
    public HashMap<String, WorkQueue> getWorkQueuesHashMap() {
        return workQueuesHashMap;
    }
    
    public WorkQueue getSepcificWorkQueue(String name){
        return this.workQueuesHashMap.get(name);
    }

    public void setWorkQueuesHashMap(HashMap<String, WorkQueue> workQueuesHashMap) {
        this.workQueuesHashMap = workQueuesHashMap;
    }
    
    public int getWorkRequestCount(){
        int count = 0;
        for(Entry<String,WorkQueue> e : workQueuesHashMap.entrySet()){
            count += e.getValue().getWorkRequestList().size();
        }
        return count;
    }
    

    @Override
    public String toString(){
        return name;
    }
}

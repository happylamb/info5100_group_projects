/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.workqueue;

import business.useraccount.UserAccount;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author xinlu
 */
public class FundingRequest extends WorkRequest{
    private Double targetNumber;
    private Map<UserAccount, Double> processNumber;
    private Double remainNumber;
    private boolean accepted = false;
    
    public FundingRequest(){
        super();
        processNumber = new HashMap<>();
    }

    public Map<UserAccount, Double> getProcessNumber() {
        return processNumber;
    }

    public void setProcessNumber(Map<UserAccount, Double> processNumber) {
        this.processNumber = processNumber;
    }
    
    public double deRemain(double amount){
         this.remainNumber -= amount;
         return remainNumber;
    }

    public Double getRemainNumber() {
        return remainNumber;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }
    
    

    public Double getTargetNumber() {
        return targetNumber;
    }

    public void setTargetNumber(Double targetNumber) {
        this.targetNumber = targetNumber;
        this.remainNumber = targetNumber;
    }
    
    @Override 
    public String toString(){
        return this.targetNumber.toString();
    }
    
}

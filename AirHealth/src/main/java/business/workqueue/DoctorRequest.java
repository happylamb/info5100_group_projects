/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.workqueue;

import business.user.User;
import business.useraccount.UserAccount;

/**
 *
 * @author xinlu
 * 
 * this is a request form doctor to medicine manage
 */
public class DoctorRequest extends WorkRequest{
    private String medicineName;
    private int quantity;
    private UserAccount patient;

    public String getMedicineName() {
        return medicineName;
    }

    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public UserAccount getPatient() {
        return patient;
    }

    public void setPatient(UserAccount patient) {
        this.patient = patient;
    }
    @Override
    public String toString(){
        return medicineName;
    }
}

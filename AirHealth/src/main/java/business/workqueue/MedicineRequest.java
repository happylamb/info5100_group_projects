/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.workqueue;

import business.user.charity.MedicineList;


/**
 *
 * @author xinlu
 * 
 * this is a request from medicine manage to pharmacy
 */

public class MedicineRequest extends WorkRequest{
    public String medicineName;
    public int quantity;

    public String getMedicineName() {
        return medicineName;
    }

    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    
    @Override
    public String toString(){
        return medicineName;
    }

}

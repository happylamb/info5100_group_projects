/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.workqueue.others;

import static business.organization.Organization.Type.Patients;
import business.user.doctor.Doctor;
import business.user.patients.Patients;
import business.useraccount.UserAccount;
import business.workqueue.WorkRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author gaomc
 */
public class QuestionWorkRequest extends WorkRequest{
    private Patients ask_patient;
    private Doctor answer_doctor;
    private Boolean finished = false;
    private List<QuestionEntity> question_answer_flow;
    private String topic;
    
    private static int count;
    
    private int rating = 0;
    
    public QuestionWorkRequest(UserAccount userAccount){
        super();
        super.setSender(userAccount);
        this.ask_patient = (Patients) userAccount.getUser();
        this.count = 0;
        this.question_answer_flow = new ArrayList<>();
        this.rating = 100;
//        this.question_answer_flow.put(this.questionEntity, time);
    }
    
    public void askQuestion(String question){
        this.question_answer_flow
                .add(new QuestionEntity(question,this.ask_patient));
        this.setStatus("unhandled");
    }
    
    public QuestionEntity answerQuestion(Doctor doctor, String answer){
        this.answer_doctor = doctor;
        QuestionEntity qe = new QuestionEntity(answer, doctor);
        this.question_answer_flow.add(qe);
        this.setStatus("processing");
        return qe;
    }
    
    public void patientConfirmQuestion(){
        this.setStatus("completed");
    }
    
    public List< QuestionEntity> getQuestionFlow(){
        return this.question_answer_flow;
    }
    
    public void setDoctor(Doctor doc){
        this.answer_doctor = doc;
    }
    
    public void setFinished (){
        this.finished = true;
        super.setStatus("completed");
    }
    
    public boolean getFinished(){
        return this.finished;
    }


    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }
    
    public String getPatientName(){
        return ask_patient.getName();
    }
    
    public Patients getPatient(){
        return this.ask_patient;
    }
    
    @Override
    public String toString(){
        return this.topic;
    }
    
    public void setRating(int rating){
        this.rating = rating;
        this.answer_doctor.setRating(rating);
    }
    
    public int getRating(){
        return this.rating;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.workqueue.others;

import business.user.User;
import java.time.LocalDateTime;
import java.util.Date;

/**
 *
 * @author gaomc 
 * modified by wamg.mengh ( add poseTime)
 */
public class QuestionEntity {
    String question_answer;
    User holder;
    private LocalDateTime time;

    
    
    public QuestionEntity(String que_ans, User holder){
        this.question_answer = que_ans;
        this.holder = holder;
        this.time = LocalDateTime.now();
    }
    
    public QuestionEntity getEntity(){
        return this;
    }

    public String getQuestion_answer() {
        return question_answer;
    }

    public User getHolder() {
        return holder;
    }
    
    public LocalDateTime getTime(){
        return this.time;
    }
    
    public String getName(){
        return holder.getName();
    }
    
    public Boolean isDoctor(){
        Boolean isDoctor = false;
        if(holder.getUserType().equals("VolunteerDoctor")){
            isDoctor = true;
        }else if(holder.getUserType().equals("Patients")){
            isDoctor = false;
        }
        return isDoctor;
    }
    
    @Override
    public String toString(){
        return this.question_answer;
    }
}

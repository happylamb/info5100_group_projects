/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.workqueue.others;

import static business.organization.Organization.Type.Patients;
import static business.role.Role.RoleType.Patient;
import business.user.charity.MedicineList;
import business.user.patients.Patients;
import business.useraccount.UserAccount;
import business.workqueue.WorkRequest;

/**
 *
 * @author gaomc
 */
public class NotificationWorkRequest extends WorkRequest {

    private MedicineList medicineList;
    private String listNo;
    private String msg;
    
    
    public NotificationWorkRequest(UserAccount sender, UserAccount patient) {
        super();
        super.setSender(sender);
        super.setReceiver(patient);
    }
  
    public MedicineList getMedicineList() {
        return medicineList;
    }

    public void setMedicineList(MedicineList medicineList) {
        this.medicineList = medicineList;
    }

    public String getListNo() {
        return listNo;
    }

    public void setListNo(String listNo) {
        this.listNo = listNo;
    }

    public void setMessage(MedicineList medicineList) {
        this.medicineList= medicineList;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    
    
    
}

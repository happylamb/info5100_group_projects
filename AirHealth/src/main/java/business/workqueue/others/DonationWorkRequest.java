/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.workqueue.others;

import business.enterprise.Enterprise;
import business.useraccount.UserAccount;
import business.workqueue.WorkRequest;
import java.time.LocalDateTime;

/**
 *
 * @author gaomc
 */
public class DonationWorkRequest extends WorkRequest {
    
    private double amount;
    private Enterprise enterprise;
    private static LocalDateTime time;
    
    public DonationWorkRequest(UserAccount userAccount, int amount){
        super();
        super.setSender(userAccount);
        super.setStatus("unhandled");
        this.enterprise = userAccount.getEnterprise();
        this.amount = amount;
        this.time = LocalDateTime.now();
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
    
    @Override
    public String toString(){
        return Double.toString(amount);
    }
    
}

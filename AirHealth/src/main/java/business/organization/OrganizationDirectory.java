/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.organization;

import business.organization.Organization.Type;
import business.organization.government.GovernmentAdminOrganization;
import business.organization.government.VolunteerFinanceOrganization;
import business.organization.hospital.ClinicOrganization;
import business.organization.hospital.HospitalAdminOrganization;
import business.organization.patients.PatientsOrganization;
import business.organization.pharmacy.CharityOrganization;
import business.organization.pharmacy.PharmacyAdminOrganization;
import business.organization.volunteer.FinanceOrganization;
import business.organization.volunteer.MedicineManageOrganization;
import business.organization.volunteer.VolunteerAdminOrganization;
import business.organization.volunteer.VolunteerDoctorOrganization;
import java.util.ArrayList;

/**
 *
 * @author xinlu
 */
public class OrganizationDirectory {
    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    
    public Organization createOrganization(Type type){
        Organization organization = null;
        switch(type.getValue()){
            case "Charity Organization":
                organization = new CharityOrganization();
                organizationList.add(organization);
                break;
            case "Finance Organization":
                organization = new FinanceOrganization();
                organizationList.add(organization);
                break;
            case "GovernmentAdmin Organization":
                organization = new GovernmentAdminOrganization();
                organizationList.add(organization);
                break;
            case "MedicineManage Organization":
                organization = new MedicineManageOrganization();
                organizationList.add(organization);
                break;
            case "PharmacyAdmin Organization":
                organization = new PharmacyAdminOrganization();
                organizationList.add(organization);
                break;
            case "VolunteerAdmin Organization":
                organization = new VolunteerAdminOrganization();
                organizationList.add(organization);
                break;
            case "VolunteerDoctor Organization":
                organization = new VolunteerDoctorOrganization();
                organizationList.add(organization);
                break;
            case "VolunteerFiance Organization":
                organization = new VolunteerFinanceOrganization();
                organizationList.add(organization);
                break;
            case "Patients":
                organization = new PatientsOrganization();
                organizationList.add(organization);
                break;
            default: 
                break;
        }

        return organization;
    }
}

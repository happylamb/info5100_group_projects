/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.user.doctor;

import business.role.Role;
import business.user.User;
import business.workqueue.WorkQueue;
import business.workqueue.others.QuestionWorkRequest;

/**
 *
 * @author gaomc
 */
public class Doctor extends User{

    private String qulification_number;
    private String qulification_type;
    private String hospital_info;
    private WorkQueue answerHistoryQueue;
    private double rating = 3;
    private boolean isApprove;
    private double ratingAmount;
    private static int countRating = 0;
    
    private String department;
    
    public Doctor() {
        super(Role.RoleType.VolunteerDoctor.getValue());
        this.answerHistoryQueue = new WorkQueue();
        this.isApprove = false;
        this.ratingAmount = 3;
    }
    
    public WorkQueue getAnswerHistoryQueue(){
        return answerHistoryQueue;
    }
    
    public void addToHistoryQueue(QuestionWorkRequest qWR){
        answerHistoryQueue.addWorkRequest(qWR);
    }

    public String getQulification_number() {
        return qulification_number;
    }

    public void setQulification_number(String qulification_number) {
        this.qulification_number = qulification_number;
    }

    public String getQulification_type() {
        return qulification_type;
    }

    public void setQulification_type(String qulification_type) {
        this.qulification_type = qulification_type;
    }

    public String getHospital_info() {
        return hospital_info;
    }

    public void setHospital_info(String hospital_info) {
        this.hospital_info = hospital_info;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public boolean isIsApprove() {
        return isApprove;
    }

    public void setIsApprove(boolean isApprove) {
        this.isApprove = isApprove;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        countRating ++;
        this.ratingAmount += rating;
        this.rating = this.ratingAmount/countRating;
    }
    
    
    

    @Override
    public String toString(){
        return this.getName() + "; " + this.getUserType().toString();
    }
    
}

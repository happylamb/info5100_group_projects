/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.user.charity;

import java.util.Date;

/**
 *
 * @author xinlu
 */
public class Medicine {
    String name;
    String description;
    String categoty;
    public Medicine(String name,String description,String category){
        this.name = name;
        this.description = description;
        this.categoty = category;

    }

    public Medicine() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategoty() {
        return categoty;
    }

    public void setCategoty(String categoty) {
        this.categoty = categoty;
    }

    @Override
    public String toString(){
        return name;
    }
}

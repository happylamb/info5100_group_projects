/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.user.charity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author xinlu
 */
public class MedicineList {
    private Map<String,Integer> medicineList;
    
    public MedicineList(){
        medicineList = new HashMap<>();
    }

    public Map<String, Integer> getMedicineList() {
        return medicineList;
    }

    public void setMedicineList(Map<String, Integer> medicineList) {
        this.medicineList = medicineList;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.user.patients;

import business.role.Role;
import business.user.User;
import java.util.ArrayList;

/**
 *
 * @author gaomc
 */
public class Patients extends User{
    

    MedicalRecordsDirectory medicalRecordsDirectory;
    String medicalHistory;
    String income;
    
    public Patients() {
        super(Role.RoleType.Patient.getValue());
    }
    
    public ArrayList<MedicalRecords> getMedicalRecordsDirectory(){
        return this.medicalRecordsDirectory.getRecords();
    }
    
    public void addRecords(MedicalRecords medicalRecords){
        this.medicalRecordsDirectory.getRecords().add(medicalRecords);
    }

    public String getMedicalHistory() {
        return medicalHistory;
    }

    public void setMedicalHistory(String medicalHistory) {
        this.medicalHistory = medicalHistory;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    @Override
    public String toString(){
        return this.getName()  + " ; " + this.getUserType();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import business.db4oUtil.DB4OUtil;
import business.enterprise.Enterprise;
import business.network.Network;
import business.organization.Organization;
import business.role.Role;
import business.role.SystemAdminRole;
import business.workqueue.WorkQueue;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author xinlu
 */ 
public class AirDoctorVolunteerPlatform extends Organization{
    private ArrayList<Network> networkList;
    private HashMap<String,WorkQueue> workQueuesHashMap;


    private static AirDoctorVolunteerPlatform business;
    private static DB4OUtil dB4oUtil = DB4OUtil.getInstance();
    private static AirDoctorVolunteerPlatform system = dB4oUtil.retrieveSystem();
   
    public static AirDoctorVolunteerPlatform getInstance(){
        if(system == null){
            system = new AirDoctorVolunteerPlatform();
            system = ConfigureSystem.configure(system);
        }   
        return system;
    }
    

    
    public Network createAndAddNetwork(){
        Network network=new Network();
        networkList.add(network);
        return network;
    }
//    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roleList=new ArrayList<Role>();
        roleList.add(new SystemAdminRole());
        return roleList;
    }
    private AirDoctorVolunteerPlatform(){
        super(Organization.Type.System);
        networkList=new ArrayList<Network>();
        this.workQueuesHashMap = new HashMap<String,WorkQueue>();
    }
    
    


    public ArrayList<Network> getNetworkList() {
        return networkList;
    }
   

    public void setNetworkList(ArrayList<Network> networkList) {
        this.networkList = networkList;
    }
    
    public boolean checkIfUserIsUnique(String userName){
        if(!this.getUserAccountDirectory().checkIfUsernameIsUnique(userName)){
            return false;
        }
        for(Network network:networkList){
            
        }
        return true;
    }
    

//    public void setWorkQueueInventory(){
//         //add workQueue for Patient
//        workQueuesHashMap.put("patient_docQuestion", new WorkQueue());
//        
//        //add workQueue for Volunteer Organization
//        workQueuesHashMap.put("doctor_contactHospital", new WorkQueue());
//        workQueuesHashMap.put("contactHospital_clinic", new WorkQueue());
//        workQueuesHashMap.put("notification_Appointment", new WorkQueue());
//        workQueuesHashMap.put("notification_Pharmacy", new WorkQueue());
//
//        
//        //add workQueue for Gov
//        workQueuesHashMap.put("finance_government", new WorkQueue());
//        
//        workQueuesHashMap.put("doctor_medicineManage", new WorkQueue());
//        workQueuesHashMap.put("medicineManage_pharmacy", new WorkQueue());
//    }
//     
//    public HashMap<String, WorkQueue> getWorkQueuesHashMap() {
//        return workQueuesHashMap;
//    }
//    
//    public WorkQueue getSepcificWorkQueue(String name){
//        return this.workQueuesHashMap.get(name);
//    }
//
//    public void setWorkQueuesHashMap(HashMap<String, WorkQueue> workQueuesHashMap) {
//        this.workQueuesHashMap = workQueuesHashMap;
//    }
}

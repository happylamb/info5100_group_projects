/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.useraccount;
import business.enterprise.Enterprise;
import business.network.Network;
import business.user.employee.Employee;
import business.role.Role;
import business.user.User;
import business.workqueue.WorkQueue;

/**
 *
 * @author xinlu
 */
public class UserAccount {
    private String username;
    private String password;
    // user: employee & doctor & patients
    private User user;
    private Role role;
    private WorkQueue workQueue;
    private Enterprise enterprise;
    private Network network;
    private boolean approved = false;


    public UserAccount() {
        workQueue = new WorkQueue();
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public User getUser() {
        return user;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }
    
    public Enterprise getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(Enterprise enterprise) {
        this.enterprise = enterprise;
    }

    public Network getNetwork() {
        return network;
    }

    public void setNetwork(Network network) {
        this.network = network;
    }

    @Override
    public String toString() {
        return username;
    }
}
